const { Router } = require('express');

const ProjectController = require('./controllers/ProjectController');

const routes = Router();

routes.route('/projects')
  .get(ProjectController.findAll)
  .post(ProjectController.create);

routes.route('/projects/:id')
  .put(ProjectController.update)
  .delete(ProjectController.remove);

routes.post('/projects/:id/tasks', ProjectController.createTasks);

module.exports = routes;
