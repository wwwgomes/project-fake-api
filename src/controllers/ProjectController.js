const service = require('../services/ProjectService');

exports.findAll = (req, res) => {
  const projects = service.findAll();
  res.status(200).json({ projects });
}

exports.create = (req, res) => {
  const project = service.create(req.body);

  return res.status(201).json(project);
}

exports.update = (req, res) => {
  const { id } = req.params;
  const project = service.update(id, req.body);

  return res.status(200).json(project);
}

exports.remove = (req, res) => {
  const { id } = req.params;
  service.remove(id);
  return res.status(204).send();
}

exports.createTasks = (req, res) => {
  const { id } = req.params;
  service.createTasks(id, req.body);

  return res.status(201).json(project);
}


