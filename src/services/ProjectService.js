const Project = require('../models/Project');

const projects = [];

let id = 0;

module.exports = {
  findAll () {
    return projects;
  },
  create (obj) {
    id += 1;
    const { title } = obj;

    const project = new Project(id, title, tasks = []);

    projects.push(project);

    return project;
  },
  update (id, obj) {
    const { title } = obj;
    const project = findById(id);
    project.title = title;

    const index = projects.indexOf(project);

    projects.splice(index, 1, project);

    return project;
  },
  remove (id) {
    const project = findById(id);
    const i = projects.indexOf(project);

    projects.splice(i, 1);
  },
  createTasks (id, obj) {
    const { title } = obj;
    const project = findById(id);

    project.tasks.push(title);

    const i = projects.indexOf(project);

    projects.splice(i, 1, project);

    return project;
  }

}

function findById (id) {
  const project = projects.find(p => p.id === Number(id));

  return project;
}
